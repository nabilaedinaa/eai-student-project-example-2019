# CSCM602023 Advanced Programming (KKI) - 2020 - Final Programming Exam

> Nabila Edina - 1806173531

#### Task 1
The issues:
1. Config: The text analysis API key was exposed in the application.properties
    * I added the API key into the environment variables like this:
    ![appproperties](images/image1.png)
    * I changed the TextAnalysisServiceImpl.java files so that it won't be exposed anymore, like this:
    ![appproperties](images/image2.png)

2. Logs: 
    * I added annotation @Log and log.info


#### Task 2
Prometheus:
![appproperties](images/image3.png)

    
    
       


