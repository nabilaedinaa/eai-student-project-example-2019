package com.adf.tugasakhir.controller;

import java.security.Principal;

import lombok.extern.java.Log;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Log
@Controller
public class AuthController {

  @GetMapping("/login")
  @PreAuthorize("permitAll()")
  public String login(final Principal principal) {
    log.info("Login");
    if (principal != null) {
      return "redirect:/";
    }
    return "_login.html";
  }
}
